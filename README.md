wooey-multi-arch is a multi-architecture docker image for [Wooey](https://wooey.readthedocs.io/en/latest/running_wooey.html)


# wooey-multi-arch v1 - Example usage

Start Wooey:

```bash
docker run -d -p 8000:8000 -p 80:80  adrianoamalfi/wooey-multi-arch:latest
```

Connect to http://localhost:8000 
Username: admin
Password: changeme

Start a Wooey server with Postgres DB and RabbitMQ:

```bash
WIP
```

